function sis= SistEdos(t,x)
    
    CA = x(1);
    CB = x(2);
    T  = x(3);
    TM = x(4);
    Tj = x(5);

        
    E1 = 15000; %Energ�a de activaci�n (BTU/lb mol)
    E2 = 20000; %Energ�a de activaci�n (BTU/lb mol)
    R = 1.99; %Constante de los gases (Btu/lb mol �R)  <- ESTABA EN EL MATERIAL COMPLEMENTARIO


    lambda1 = -40000; %(Btu/lb mol)
    lambda2 = -50000; %(Btu/lb mol)
    rho = 50; %(lbm/ft^3)
    Cp = 1; %(Btu/lbm �R)
    hi = 160; %(Btu/ h �F ft^2)
    Ai = 56.5; %(ft2)
    V = 42.5; %(ft^3)
    A0 = 56.5; %(ft^2)
    phoM = 512; %(lbm/ft^3)
    CM = 0.12; %(Btu/lbm �R)
    VM = 9.42; %(ft^3)
    
    alpha1 = 729.55;%(min^-1) 
    alpha2 = 6567.6; % (min^-1)
    k1 = alpha1*exp(-E1/(R*T));
    k2 = alpha2*exp(-E2/(R*T));
    
    %Parte A
    h0s = 1000; %(Btu/h �F ft2)
    A0s = A0; %No estoy seguro de que sea lo mismo.
    Vj =  18.83; %(ft3)
    Cvs = 112; %(Btu/min psi 0.5)
    Kc = 10; %(psi/psi)
    Pset = 12; %PREGUNTAR QUE ES
    Avp = -8744.4; %(�R)
    Bvp = 15.70; %(�R) 
    Hs_c =  939; %(Btu/ lbm) 
    M = 18; %Peso Molecular del vapor
    
    %C�lculo de ws
    Pj = exp( Avp/(Tj+460-460) + Bvp );
    PTT = 3 + (T-50-460)*(12/200); %Sume 460 para convertirlo a �R
    Pc = 7 + Kc*(Pset-PTT); 
    if Pc>=9 && Pc<=15 
        Xs = (Pc-9)/6;
    elseif Pc<9
        Xs = 0;
    else 
        Xs = 1;
    end    
    ws = Cvs*Xs*sqrt(abs(35-Pj)); 
    
    %C�lculo de wc
    Qj = -h0s*A0s*(Tj-TM); 
    wc = -Qj/(Hs_c);
    
    %C�lculo de d(pho_s)/dt
    dphos = -M*(Avp+Tj+460-460)*Pj/( R*(Tj+460-460)^3 );
    dTj = (ws-wc)/(Vj*dphos);

    
    %Sistema de EDOs principal
    dCA = -k1*CA;
    dCB = k1*CA-k2*CB;
    dT = -lambda1/(rho*Cp)*k1*CA-lambda2/(rho*Cp)*k2*CB-hi*Ai*(T-TM)/(V*rho*Cp);
    dTM = (hi*Ai*(T-TM)-Qj)/(phoM*CM*VM);
    
     
    sis = [dCA;dCB;dT;dTM;dTj];

    
end
function sis= SistEdosB(t,x,t200)
    
    CA = x(1);
    CB = x(2);
    T  = x(3);
    TM = x(4);
    Tj = x(5);
    Vj = x(6);


        
    E1 = 15000; %Energ�a de activaci�n (BTU/lb mol)
    E2 = 20000; %Energ�a de activaci�n (BTU/lb mol)
    R = 1.99; %Constante de los gases (Btu/lb mol �R)  <- ESTABA EN EL MATERIAL COMPLEMENTARIO


    lambda1 = -40000; %(Btu/lb mol)
    lambda2 = -50000; %(Btu/lb mol)
    rho = 50; %(lbm/ft^3)
    Cp = 1; %(Btu/lbm �R)
    hi = 160; %(Btu/ h �F ft^2)
    Ai = 56.5; %(ft2)
    V = 42.5; %(ft^3)
    A0T = 56.5; %(ft^2)
    phoM = 512; %(lbm/ft^3)
    CM = 0.12; %(Btu/lbm �R)
    VM = 9.42; %(ft^3) 
    alpha1 = 729.55;%(min^-1) 
    alpha2 = 6567.6; % (min^-1)
    k1 = alpha1*exp(-E1/(R*T));
    k2 = alpha2*exp(-E2/(R*T));
    
    
    %Constantes Parte B
    how =  400; %(Btu/h �F ft2) 
    rhoj =  62.3; %(lbm/ft3)
    Cj =  1.0; %(Btu/lbm �R)
    Tj0 = 80 + 460; %(�R)
    Cvm = 100; %(gpm/psi^0.5)
    VjT =  18.83; %(ft3)
    Kc = 10; %(psi/psi)

    %C�lculo Qj 
    A0 = (A0T/VjT)*Vj;
    Qj = how*A0*(TM-Tj); 
    
    
    %C�lculo Tj
    Ra = 0.1;
    Pset = 12+Ra*(t-t200); %Dejar en 12 en la parte A
    PTT = 3 + (T-50-460)*(12/200);
    Pc = 7 + Kc*(Pset-PTT); 
    if Pc>=3 && Pc<=9 
        Xw = (9-Pc)/6;
    elseif Pc<3
        Xw = 1;
    else 
        Xw = 0;
    end    
    Fw = Cvm*Xw*sqrt(20);
    dTj = ( (Tj0-Tj)*Fw + Qj/(rhoj*Cj) )/Vj;
   
    
    %C�lculo Vj
    dVj = Fw;

    %Sistema de EDOs principal
    dCA = -k1*CA;
    dCB = k1*CA-k2*CB;
    dT = -lambda1/(rho*Cp)*k1*CA-lambda2/(rho*Cp)*k2*CB-hi*Ai*(T-TM)/(V*rho*Cp);
    dTM = (hi*Ai*(T-TM)-Qj)/(phoM*CM*VM);
    
     
    sis = [dCA;dCB;dT;dTM;dTj;dVj];

    
    
end
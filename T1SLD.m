% batch_steadyNM.m
clear all; clc; close all;
syms f1 f2 f3 f4 k1 k2 cA cB 
% ODEs
f1=-k1*cA;
f2=-k1*cA-k2*cB;
%f3=k1*cA*cB-k2*cB*cC;
%f4=k2*cB*cC;
% matrix of ODEs
Fx = [f1;f2];
% matrix of concentrations
Xp = [cA;cB];
Xn = [cA;cB];
% Jacobian of ODEs and concentrations
Jx = jacobian(Fx',Xp);
% obtain input for tolerance
e =0.001 ;%% 
error = e*ones([2 1]);


a1=729.55;
a2=6567.6;
E1=15000;
E2=20000;
R= 1.987;
%T=?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ETAPA 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Vj=18.83;
Cvs=112;
Avp= -8744.4;
Bvp= 15.70;
M= 18;
Hshc= 939;
hos= 1000;
Ao= 56.5;
roj= 62.3;
Aw=-8744.4;
Bw=1570;
Kc=10;
%Pj= 35;
Pset=200;
Ptt=3+(T-50)*(12/200);
Pc=7+Kc(Pset-Ptt)
Xs= (Pc-9)/6
Xw=(9-Pc)/6
ws= Cvs*Xs

%REF=[T]
%F=@(T)myfun(M/(R*T*roj)*exp(((Aw/T)+Bw)));
[x,fval] =fsolve(@(T)(M./(R.*T.*roj).*exp((Aw./T)+Bw)),0.1)













%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ETAPA 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
how=400;
Cvw=100;
Fwo=Cvw*Xw*sqrt(20);
Qj=Ao*how*(Tm-Tj) %%% Tm Tj?


























%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ETAPA 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%










% Kinetic constants
%k1=a1*exp(-E1/R*T);  %[1/h]
%k2=a1*exp(-E2/R*T);  

% obtain initial values
%c = input('Enter initial guess\n');%%ESCRIBIR CONCENTRACIONES
cA=10;
cB=20;
%cC=c(3);
%cD=c(4);
% rate constants %%ESCRIBIR CONSTANTES
%k1=1;
%k2=0.1;



% iterate to obtain steady state values
%while('true')
%P = eval(Xp);
%N = eval(Xn);
%F = eval(Fx);
%J = eval(Jx);
%P = N;
%N = P - (pinv(J)*F);
%cA=N(1);
%cB=N(2);
%cC=N(3);
%cD=N(4);
 %   if(abs(N-P)<error)
  %      break
  %  end
%end
% display steady state concentrations
%cA
%cB
%cC
%cD

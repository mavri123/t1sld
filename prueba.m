%syms u(t) v(t)
%ode1 = diff(u) == 3*u + 4*v;
%ode2 = diff(v) == -4*u + 3*v;
%odes = [ode1; ode2];
%S = dsolve(odes);
%uSol(t) = S.u
%vSol(t) = S.v

syms  Ca Cb T Tm 

%Qm=hi*Ai*(T-Tm)
%ode1 = diff(Ca) == -k1*Ca
%ode2 = diff(Cb) == -k1*Ca-k2*Cb;
%ode3 = diff(T) == (1/(ro*Cp))*(-l1*k1*Ca-l2*k2*Cb-(1/V)*(hi*Ai*(T-Tm)))
%ode4 = diff(Tm) ==  (1/(rom*Cm*Vm))*( hi*Ai*(T-Tm)-Qj)
%odes = [ode1; ode2];ode3;ode4;



M=18;
R=1.987;
Avp = -8744.4;
Bvp = 15.70;
Pj = 35;
Aw=-8744.4;
Bw=1570;
roj= 62.3;



%f=@(T) (M/(R*T))*exp((Aw/T)+Bw)-roj;

syms T
f= (M/(R*T))*exp((Aw/T)+Bw)-roj;
fp= diff(f)
f= matlabFunction(f)
fp = matlabFunction(fp)
%fpp = double(fpp)

%r=fsolve(@(T) F(T),2);




x0 = 6; 
N = 10; 
tol = 1E-10;
x(1) = 1; % Set initial guess
n = 2; 
nfinal = N + 1; 
while (n <= N + 1)
  fe = f(x(n - 1));
  fpe = fp(x(n - 1));
  x(n) = x(n - 1) - fe/fpe;
  if (abs(fe) <= tol)
    nfinal = n; 
    break;
  end
  n = n + 1;
end






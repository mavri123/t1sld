%%%DATOS GLOBALES%%%%%%%%%%%%%%%%%%
R = 1.987;
l1 = -40000;
l2 = -50000;
ro = 50;
Cp= 1;
rom=512; 
Cm=0.12;
Vm=9.42;
V=42.5;
hi=160;
Ai=56.5;
hos = 1000;
Ao = 56.5;
Aw= -8744.4;
Bw= 15.7;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%      ETAPA 1     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DATOS DE LA ETAPA 1
Vj = 18.83;
Cvs = 112;
Avp = -8744.4;
Bvp = 15.70;
M = 18;
Hshc = 939;
roj = 62.3;
Kc = 10;
Pset = 12;%%%% PREGUNTAR EL VALOR DE ESTo


%%%%%%%%%%%%%CONDICIONES INICIALES
Ca=0.8;
Cb=0;
T=80;
Tj=80;
Tm=81;
Pj=35;
ros= (M*Pj)/R*(Tj+460);
Qj=-hos*Ao*(Tj-Tm);   
wc=-Qj/(Hshc);
%%%%%%%%%%%%%%%%%%%%%%
 x0= [Ca Cb T Tm];

tspan=[0,1];
for t=1:2000
    temp=T;
    
    if T <= 200
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%% PRIMERAS 5 ECS %%%ver como resolver las edos%
   
    
    [tt,x] = ode45(@(t,x)sistemaedos(t,x,temp,Tj), tspan, x0)
    tspan = [t t+1]; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5%%%%%%%%%%%%%%%%%%%%%%
    Ca=x(end,1);
    Cb=x(end,2);
    T=x(end,3);
    Tm=x(end,4);
    x0= [Ca Cb T Tm];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%PARA CALCULAR Xs%%%
    Ptt = 3+(T-50)*(12/200);
    Pc = 7+Kc*(Pset-Ptt);  %%%% FALTA PSET
    Xs = (Pc-9)/6;
    %%%%%%%%%%%%%%%%%%%%%%% 
    %%% NECESARIO PARA LAS 1AS 5 ECS%%%
   
    
   
    %edo2=[(1/Vj)*(ws-wc)]
   
  
     %%%%%%%PARA CALCULAR SIGUIENTE PJ%%%%
    
     ws = Cvs*Xs*sqrt(35-Pj);
    
    %%%%%% PA CALCULAR EL TJ
    droj= (ws-wc)*(1/Vj); 
    [tr,xr]= ode45(@(roj,t)(ws-wc)*(1/Vj),tspan,[roj]);
    roj=xr(end,1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    f= @(Tj) roj-(M/R*Tj)*exp(Aw/Tj +Bw);
    Tjj= fsolve(f,80);
    Pj=exp(Avp/(Tjj+460)+Bvp);
    ros= (M*Pj)/R*(Tj+460);
    wc= hos*Ao*(Tj-Tm)/(Hshc);
    %Qj=-hos*Ao*(Tjj-Tm);  
     
     
     %%%%%%%%%%5%%%%%%%%%%%%%%%%%%%%% 
        
    end
    
    
    
    
    
end













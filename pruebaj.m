clear all; clc;

%Condiciones iniciales:
CA0 = 0.8; %(lb mol A/ft3)
CB0 = 0; %(lb mol A/ft3)
T0 = 80+460; %(�R)
TM0 = 80+460; %(�R)
Tj0 = 80+460; %(�R)
x0 = [CA0 CB0 T0 TM0 Tj0];

t200 = 0.7455;
T = [0,t200];
[t,x] = ode45(@(t,x)SistEdos(t,x),T,x0);

CA = x(:,1);
CB = x(:,2);
T  = x(:,3);
TM = x(:,4);
Tj = x(:,5);

%%Parte B

CA0 = CA(end);
CB0 = CB(end); %(lb mol A/ft3)
T0 = T(end); %(�R)
TM0 = TM(end); %(�R)
Tj0 = Tj(end); %(�R)
Vj0 = 0.0001; % MIENTRAS
x0 = [CA0 CB0 T0 TM0 Tj0 Vj0];

VjT = 18.83; %ft3
tllenado = 100;
J = 1; j = 1;
Vj = inf;
while abs(Vj(J(j))-VjT)>0.01 %Calcula el instante de tiempo en que se llena
    
    T = [0,tllenado+0.0001];
    [t2,x2] = ode45(@(t,x)SistEdosB(t,x,t200),T,x0);

    Vj = x2(:,6);
    J = find(Vj>VjT);
    j = find(min(Vj(J(j))));
    tllenado = t2(J(j)); %tiempo en que se llena de agua
    
end    


T = [0,tllenado];
[t2,x2] = ode45(@(t,x)SistEdosB(t,x,t200),T,x0);


x = [x zeros(size(x,1),1);x2];
t2 = t2 + t200;
t = [t;t2];

CA = x(:,1);
CB = x(:,2);
T  = x(:,3);
TM = x(:,4);
Tj = x(:,5);
Vj = x(:,6);


%Parte C
CA0 = CA(end);
CB0 = CB(end); %(lb mol A/ft3)
T0 = T(end); %(�R)
TM0 = TM(end); %(�R)
Tj0 = Tj(end); %(�R)
x0 = [CA0 CB0 T0 TM0 Tj0];

T = [0,30];
[t2,x2] = ode45(@(t,x)SistEdosC(t,x,t200),T,x0);


x = [x;x2 VjT*ones(size(x2,1),1)];
t2 = t2 + t200 + tllenado;
t = [t;t2];

CA = x(:,1);
CB = x(:,2);
T  = x(:,3);
TM = x(:,4);
Tj = x(:,5);
Vj = x(:,6);


h = figure();
h.WindowState = 'maximized';
subplot(2,3,1)
plot(t,CA)
title('C_A')

subplot(2,3,2)
plot(t,Vj)
title('V_j')

subplot(2,3,3)
plot(t,CB)
title('C_B')

subplot(2,3,4)
plot(t,T-460)
title('T: Temperatura reactor')

subplot(2,3,5)
plot(t,TM-460)
title('T_M: Temperatura paredes tanque')

subplot(2,3,6)
plot(t,Tj-460)
title('T_j: Temperatura chaqueta')


%% EJERCICIO c)
%(CA,CB)
f = figure();
f.WindowState = 'maximized';
subplot(2,3,1)
plot(t,[CA CB])
title('Concentraciones')
legend('C_A','C_B')
xlabel('Tiempo (min)')
ylabel('Concentraci�n (lb mol/ft^3)')

%(T,TM,Tj)
subplot(2,3,2)
plot(t,[T-460 TM-460 Tj-460])
title('Temperaturas del Sistema')
legend('Temperatura reactor','Temperatura Paredes','Temperatura Chaqueta')
xlabel('Tiempo (min)')
ylabel('Temperatura (�F)')

%(Fwo)
subplot(2,3,3)
title('Flujo de ingreso de agua')

%(Pc,Xs,Xw)
subplot(2,3,4)
title('Se�ales v�lvulas')

%(QM)
subplot(2,3,5)
title('Calor disipado por la muralla')

%(Pset,PTT,Pc)
subplot(2,3,6)
title('Se�ales')



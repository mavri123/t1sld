function sis= sistemaedos(t,x,temp,Tj)
%%%%%%%%%%%% x(1)= Ca, x(2)=Cb, x(3)=T, x(4)=Tm
    %sig    = 10;
    %beta   = 8/3;
    %rho    = 28;
    %xprime = [-sig*x(1) + sig*x(2); 
     %          rho*x(1) - x(2) - x(1)*x(3); 
      %        -beta*x(3) + x(1)*x(2)];   
     
  % syms  Ca Cb T Tm 
     k1 = 729.55*exp(-15000/(1.987*(x(3)+460)));
     k2 = 6567.6*exp(-20000/(1.987*(x(3)+460)));  
     l1=100;%
     l2=100;%
     %Qj=200;%%%%%%%%%%%
     ro=50; 
     Cp=1; %
     V=42.5;
     hi=160; %
     Ai=56.5;%
     rom=512; %
     Cm=0.12;%
     Vm=9.42;%
     hos = 1000;
    Ao = 56.5;
   
   sis=[-k1*x(1);
         k1*x(1)-k2*x(2);
        (1/(ro*Cp))*(-l1*k1*x(1)+l2*k2*x(2)-(1/V)*(hi*Ai*(x(3)-x(4))));
        (1/(rom*Cm*Vm))*( hi*Ai*(x(3)-x(4))+hos*Ao*(Tj-x(4))) ];

   

end